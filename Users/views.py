from django.shortcuts import render
import requests
import json

from django.contrib.auth import authenticate, login, user_logged_out    # django 账号密码验证方法 登陆 注销用户
from django.contrib.auth.backends import ModelBackend   # 导入账号验证方法
from django.contrib.auth.hashers import make_password   # 对password加密

from django.db.models import Q  # 实现函数做 or 传参 get(Q(username=username)| Q(email=username))

from django.core.paginator import Paginator
from rest_framework.views import APIView
from django.http import JsonResponse

from Users.models import UserProfile, All_Perm, Perm_Group, role
from Users.serializers import All_PermSerializers, Perm_GroupSerializers, UserProfileSerializers ,RoleSerializers
from Other.models import Constructionledger
from Other.serializers import ConstructionledgerSerializers

import urllib3
from datetime import datetime
import time
from django.views.decorators.csrf import csrf_exempt

# 自定义登陆验证模式
class CustomBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs): # 自动调用验证方法
        try:
            # 根据用户名查询用户是否存在 get失败  有两个原因 有两个相同User或者没有User
            # 验证username 与 邮箱
            user = UserProfile.objects.get(Q(username=username) | Q(email=username))

            if user.check_password(password):  # user 重载了AbstractUser 验证密码  （因为密码加了密）
                return user
            if user.password == password:
                return user
        except Exception as e:  # 异常处理 验证失败
            return None


class CreateUser_api(APIView):
    def get(self, request):
        obj = role.objects.all()
        obj = RoleSerializers(instance=obj, many=True)

        return JsonResponse({'data':obj.data})


    @csrf_exempt
    def post(self, request):
        res = {}
        try:
            if request.data['params']:
                is_user = UserProfile.objects.filter(username=request.data['params']['username'])

                if is_user:
                    res['code'] = 1001
                    res["msg"] = "用户已经存在！请重新输入新账号。"
                    return JsonResponse(res)

                user = UserProfile()
                user.username = request.data['params']['username']
                user.name = request.data['params']['name']
                if request.data['params']. get('role'):
                    user.role = request.data['params']['role']
                else:
                    user.role = "visitor"
                user.email = request.data['params']['mail']
                user.password = make_password(request.data['params']['password'])
                user.save()
                res['code'] = 200
                res['msg'] = '用户注册成功!'
                return JsonResponse(res)
            else:
                res['code'] = 1001
                res['msg'] = '上传数据为空!'
                return JsonResponse(res)
        except:

            res['code'] = 1001
            res['msg'] = '服务器繁忙，请稍后尝试!'
            return JsonResponse(res)



class Login_api(APIView):
    def get(self, request):
        Data = {}
        user_name = request.GET.get("username", "")
        pass_work = request.GET.get("password", "")
        type = request.GET.get("type", "")
        user = authenticate(username=user_name, password=pass_work)

        if user is not None:
            login(request, user)  # 将user信息压入request
            Data["code"] = 200
            Data['type'] = type
            Data["User"] = user_name
            Data["Character"] = user.role
            Perm = Perm_Group.objects.filter(role=user.role)
            data = Perm_GroupSerializers(instance=Perm, many=True)
            tag = []
            for i in data.data:
                tag.append(i["Model_tag"])
            Data["superuser"] = user.is_superuser
            Data["Permission"] = tag
        else:
            Data["code"] = 300
            Data["msg"] = "账号或密码错误！"

        return JsonResponse(Data)

    @csrf_exempt
    def post(self, request, num, page):
        Data = {}
        user_name = request.POST.get("username", "")
        pass_work = request.POST.get("password", "")
        user = authenticate(username=user_name, password=pass_work)

        if user is not None:
            login(request, user)  # 将user信息压入request
            Data["code"] = 200
            Data["User"] = user_name
            Data["Character"] = "Visitor"
            Perm = Perm_Group.objects.filter(UserName=user_name)
            data = Perm_GroupSerializers(instance=Perm, many=True)
            tag = []
            for i in data.data:
                tag.append(i["Model_tag"])
            Data["Permission"] = tag
        else:
            Data["code"] = 300
            Data["msg"] = "账号或密码错误！"

        return JsonResponse(Data)



class UserList_api(APIView):
    def get(self, request):
        try:
            obj = UserProfile.objects.all()
            ser_obj = UserProfileSerializers(instance=obj, many=True)
            Paginator_list = Paginator(ser_obj.data, int(request.GET.get("row_num", "")))
            page_list = Paginator_list.page(int(request.GET.get("page", "")))

            all = role.objects.all()
            all = RoleSerializers(instance=all, many=True)
            data = {}
            data['all_role'] = all.data
            data['user_list'] = page_list.object_list
            data['Page_total'] = Paginator_list.count

            return JsonResponse(data)
        except:
            data = {}
            data['code'] = 210
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)

    @csrf_exempt
    def post(self, request):
        try:
            res = {}
            type = request.data['params']['type']
            if(type == 'change'):
                username = request.data['params']['username']
                email = request.data['params']['email']
                name = request.data['params']['name']
                role = request.data['params']['role']

                user = UserProfile.objects.get(username=username)
                user.email = email
                user.name = name
                user.role = role
                user.save()
                res['code'] = 200
                res['msg'] = '修改成功！'
            elif(type == 'delete'):
                username = request.data['params']['username']
                del_user = UserProfile.objects.get(username=username)
                del_user.delete()
                data = {}
                data['code'] = 200
                data['msg'] = '删除成功！'
                return JsonResponse(data)
        except:
            res['code'] = 200
            res['msg'] = '服务器内部错误！'

        return JsonResponse(res)



class ChangePwd_api(APIView):
    def get(self, request):
        return JsonResponse({'msg': '支持Get请求！'})


    @csrf_exempt
    def post(self, request):
        try:
            data = {}
            user = UserProfile.objects.get(username=request.data['params']['username'])
            user.password = make_password(request.data['params']['password'])
            user.save()
            data['code'] = 200
            data['msg'] = '修改成功！'
            return JsonResponse(data)
            pass
        except:
            data['code'] = 210
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)



class Role_api(APIView):
    def get(self, request):
        obj = role.objects.all()
        ser_obj = RoleSerializers(instance=obj, many=True)
        return JsonResponse({'data': ser_obj.data})

    @csrf_exempt
    def post(self, request):
        try:
            Data = {}
            role = request.data['role']
            all_prem = All_Perm.objects.all()
            all_prem = All_PermSerializers(instance=all_prem, many=True)
            perm_grrm = Perm_Group.objects.filter(role=role)
            perm_grrm = Perm_GroupSerializers(instance=perm_grrm, many=True)

            Data['code'] = 200
            Data["perm_grrm"] = perm_grrm.data
            Data["all_prem"] = all_prem.data
            return JsonResponse(Data)
        except:
            Data = {}
            Data['code'] = 200
            Data['code'] = '服务器繁忙，请稍后再试！'
            return JsonResponse(Data)



class UpdateRole_api(APIView):
    def get(self, request):
        return JsonResponse({'data': "hello"})

    @csrf_exempt
    def post(self, request):
        list = request.GET.get("list", "")
        role = request.GET.get("role", "")
        list = list.split('-')
        Perm_Group.objects.filter(role=role).delete()

        for i in list:
            obj = All_Perm.objects.get(Pern=int(i))
            perm_g = Perm_Group()

            perm_g.role = role
            perm_g.Perm = obj.Pern
            perm_g.description =obj.description
            perm_g.Model_tag =obj.Model_tag
            perm_g.save()

        return JsonResponse({'data': "完成"})


