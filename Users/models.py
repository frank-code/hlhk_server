# _*_encoding:utf-8 _*_
from __future__ import unicode_literals


from django.db import models
from django.contrib.auth.models import AbstractUser


class UserProfile(AbstractUser):
    name = models.CharField(max_length=50,verbose_name=u"名称", default="" )
    role = models.CharField(max_length=20, verbose_name=u"角色", default="")

    class Meta:
        verbose_name = "用户信息"
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.name


# 权限组
class Perm_Group(models.Model):
    role = models.CharField(max_length=20, verbose_name=u"角色", default="")
    Perm = models.IntegerField(default=0, verbose_name=u"权限id")
    description = models.CharField(max_length=50, verbose_name=u"描述", default="")
    Model_tag = models.CharField(max_length=40, verbose_name=u"模型名-唯一", default="")


# 具体某个权限
class All_Perm(models.Model):
    Pern = models.IntegerField(default=0, verbose_name=u"权限")
    description = models.CharField(max_length=50, verbose_name=u"描述", default="")
    Model_tag = models.CharField(max_length=40, verbose_name=u"模型名-唯一", default="")


class role(models.Model):
    role = models.CharField(max_length=20, verbose_name=u"角色", default="")
    description =  models.CharField(max_length=20, verbose_name=u"描述", default="")