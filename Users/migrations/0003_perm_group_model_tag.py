# Generated by Django 2.0 on 2019-10-30 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0002_all_perm_model_tag'),
    ]

    operations = [
        migrations.AddField(
            model_name='perm_group',
            name='Model_tag',
            field=models.CharField(default='', max_length=40, verbose_name='模型名-唯一'),
        ),
    ]
