# Generated by Django 2.0 on 2019-10-31 10:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0004_auto_20191031_1448'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='perm_group',
            name='UserName',
        ),
        migrations.AddField(
            model_name='perm_group',
            name='role',
            field=models.CharField(default='', max_length=20, verbose_name='角色'),
        ),
    ]
