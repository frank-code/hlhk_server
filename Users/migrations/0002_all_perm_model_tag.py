# Generated by Django 2.0 on 2019-10-30 11:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='all_perm',
            name='Model_tag',
            field=models.CharField(default='', max_length=40, verbose_name='模型名-唯一'),
        ),
    ]
