# _*_encoding:utf-8 _*_

from rest_framework import serializers
from Users.models import UserProfile, Perm_Group, All_Perm, role


class UserProfileSerializers(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = "__all__"


class Perm_GroupSerializers(serializers.ModelSerializer):
    class Meta:
        model = Perm_Group
        fields = "__all__"


class All_PermSerializers(serializers.ModelSerializer):
    class Meta:
        model = All_Perm
        fields = "__all__"


class RoleSerializers(serializers.ModelSerializer):
    class Meta:
        model = role
        fields = "__all__"