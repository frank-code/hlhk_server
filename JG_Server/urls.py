"""JG_Server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Users.views import CreateUser_api, Login_api, UserList_api, Role_api, UpdateRole_api, ChangePwd_api
from Other.views import device_api, Change_device_api, FactoryBuilding_api, Change_FactoryBuilding_Api, Tubing_Api, Gaspipe_Api, Sewage_Api, Upfile_api, Download_Api, ERP_Device_Api, ERP_Houses_Api

urlpatterns = [

    path('admin/', admin.site.urls),
    path('CreateUser/', CreateUser_api.as_view()),
    path('Login/', Login_api.as_view()),

    path('UserList/', UserList_api.as_view()),
    path('ChangePwd/', ChangePwd_api.as_view()),
    path('Role/', Role_api.as_view()),

    path('Device/', device_api.as_view()), # 设备
    path('ChangeDevice/', Change_device_api.as_view()),


    path('Factor/', FactoryBuilding_api.as_view()),
    path('ChangeFactor/', Change_FactoryBuilding_Api.as_view()),

    path('Tubing/', Tubing_Api.as_view()),
    path('Gaspipe/', Gaspipe_Api.as_view()),
    path('Sewage/', Sewage_Api.as_view()),

    path('UpRole/', UpdateRole_api.as_view()),
    path('Upfile/', Upfile_api.as_view()),
    path('Downloadfile/', Download_Api.as_view()),

    path('ERP_Device_Api/', ERP_Device_Api.as_view()),
    path('ERP_Houses_Api/', ERP_Houses_Api.as_view()),





]
