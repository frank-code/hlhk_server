from django.shortcuts import render
import requests
import urllib3
import json

from django.core.paginator import Paginator

from rest_framework.views import APIView
from django.http import JsonResponse
from django.http import HttpResponse
from django.http import FileResponse

from Other.models import Constructionledger, DeviceManagement, Oil_Pipeline, GasPipeline, SewageLineLibrary, DescriptionFile
from Other.serializers import ConstructionledgerSerializers, DeviceManagementSerializers, Oil_PipelineSerializers, GasPipelineSerializers, SewageLineLibrarySerializers

from JG_Server.settings import BASE_DIR

from datetime import datetime
import time
from django.views.decorators.csrf import csrf_exempt

#  设备
class device_api(APIView):
    def get(self, request):
        try:
            obj = DeviceManagement.objects.all()
            objdata = DeviceManagementSerializers(instance=obj, many=True)
            Paginator_list = Paginator(objdata.data, int(request.GET.get("row_num", "")))
            page_list = Paginator_list.page(int(request.GET.get("page", "")))

            data = {}

            # 班组去重
            obj = obj.values('team').distinct()
            data['search_data'] = list(obj)
            data['data'] = page_list.object_list
            data['Page_total'] = Paginator_list.count
            return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


    @csrf_exempt
    def post(self, request):
        try:
            type = request.data['type']
            # 搜索
            if(type ==  "search"):
                earch_value = request.data['value']
                obj = DeviceManagement.objects.filter(team=earch_value)
                objdata = DeviceManagementSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(request.data['row_num']))
                page_list = Paginator_list.page(int(request.data['page']))
                data = {}
                data['code'] = 200
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
            elif(type ==  "delete"):
                data = {}
                id = int(request.data['value'])
                obj = DeviceManagement.objects.get(id=id)

                if(obj == None):
                    data['code'] = 210
                    data['msg'] = '没找到该台账信息！'
                    return JsonResponse(data)
                obj.delete()
                data['code'] = 200
                data['msg'] = '成功删除该记录！'

                obj = DeviceManagement.objects.all()
                objdata = DeviceManagementSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(request.data['row_num']))
                page_list = Paginator_list.page(int(request.data['page']))
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
            else:  # 正常浏览
                obj = DeviceManagement.objects.all()
                objdata = DeviceManagementSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(request.data['row_num']))
                page_list = Paginator_list.page(int(request.data['page']))
                data = {}
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


#  修改设备表
class Change_device_api(APIView):
    def get(self, request):
        try:
            pass
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)

    @csrf_exempt
    def post(self, request):
        try:
            obj = DeviceManagement.objects.get(id=int(request.data['params']['row']['id']))

            obj.ModelName = request.data['params']['row']['ModelName']
            obj.name = request.data['params']['row']['name']
            obj.team = request.data['params']['row']['team']
            obj.level = request.data['params']['row']['level']
            obj.Thub = request.data['params']['row']['Thub']
            obj.SM = request.data['params']['row']['SM']
            obj.SerialNumber = request.data['params']['row']['SerialNumber']
            obj.UniformNumbers = request.data['params']['row']['UniformNumbers']
            obj.specification =request.data['params']['row']['specification']
            # obj.pressure = request.data['params']['row']['pressure']
            obj.UseUnit = request.data['params']['row']['UseUnit']
            obj.TimeOfUse = request.data['params']['row']['TimeOfUse']
            obj.ManufacturerName = request.data['params']['row']['ManufacturerName']
            obj.CalibrationEquipment = request.data['params']['row']['CalibrationEquipment']
            obj.Importance = request.data['params']['row']['Importance']
            obj.KeySupportEquipment = request.data['params']['row']['KeySupportEquipment']
            obj.MajorEquipment = request.data['params']['row']['MajorEquipment']
            obj.SelfRecycling = request.data['params']['row']['SelfRecycling']
            obj.SamplingValve =request.data['params']['row']['SamplingValve']
            obj.OilRelatedEquipment = request.data['params']['row']['OilRelatedEquipment']
            obj.OilEquipment = request.data['params']['row']['OilEquipment']
            obj.Medium = request.data['params']['row']['Medium']
            obj.save()

            data = {}
            data['code'] = 200
            data['msg'] = '修改成功！'
            return JsonResponse(data)
        except:
            data = {}
            data['code'] = 1000
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


#  厂房
class FactoryBuilding_api(APIView):
    def get(self, request):
        try:
            data = {}
            data['code'] = 200
            obj = Constructionledger.objects.all()
            objdata = ConstructionledgerSerializers(instance=obj, many=True)
            Paginator_list = Paginator(objdata.data, int(request.GET.get("row_num", "")))
            page_list = Paginator_list.page(int(request.GET.get("page", "")))

            obj = obj.values('PlantNumber').distinct()
            data['search_data'] = list(obj)
            data['data'] = page_list.object_list
            data['Page_total'] = Paginator_list.count
            return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


    @csrf_exempt
    def post(self, request):
        try:
            data = {}
            type = request.data['type']
            page = request.data['page']
            row_num = request.data['row_num']
            value = request.data['value']

            if (type == "search"):  # 搜索
                obj = Constructionledger.objects.filter(PlantNumber=value)
                objdata = ConstructionledgerSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(row_num))
                page_list = Paginator_list.page(int(page))
                data['code'] = 200
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
            elif (type == "delete"):
                data = {}
                id = int(request.data['value'])
                obj = Constructionledger.objects.get(id=id)

                if (obj == None):
                    data['code'] = 210
                    data['msg'] = '没找到该台账信息！'
                    return JsonResponse(data)
                obj.delete()
                data['code'] = 200
                data['msg'] = '成功删除该记录！'

                obj = Constructionledger.objects.all()
                objdata = ConstructionledgerSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(request.data['row_num']))
                page_list = Paginator_list.page(int(request.data['page']))
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
            else:
                obj = Constructionledger.objects.all()
                objdata = ConstructionledgerSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(row_num))
                page_list = Paginator_list.page(int(page))
                data['code'] = 200
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)

    def options(self, request):
        print('options')
        data = {}
        data['code'] = 200
        data['msg'] = '服务器繁忙请稍后再试！'
        return JsonResponse(data)


class Change_FactoryBuilding_Api(APIView):
    def get(self, request):
        try:
            pass
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


    @csrf_exempt
    def post(self, request):
        try:
            obj = Constructionledger.objects.get(id=int(request.data['params']['row']['id']))

            obj.ModelName = request.data['params']['row']['ModelName']
            obj.PlantNumber = request.data['params']['row']['PlantNumber']
            obj.Use = request.data['params']['row']['Use']
            obj.UseUnit = request.data['params']['row']['UseUnit']
            obj.Location =request.data['params']['row']['Location']
            obj.MgDepartment = request.data['params']['row']['MgDepartment']
            obj.TimeOfUse = request.data['params']['row']['TimeOfUse']
            obj.ConstructionArea = request.data['params']['row']['ConstructionArea']
            obj.BuildingStructure =request.data['params']['row']['BuildingStructure']
            obj.Remarks = request.data['params']['row']['Remarks']
            obj.save()
            data = {}
            data['code'] = 200
            data['msg'] = '修改成功！'
            return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


# 油库
class Tubing_Api(APIView):
    def get(self, request):
        try:
            obj = Oil_Pipeline.objects.all()
            objdata = Oil_PipelineSerializers(instance=obj, many=True)
            Paginator_list = Paginator(objdata.data, int(request.GET.get("row_num", "")))
            page_list = Paginator_list.page(int(request.GET.get("page", "")))
            data = {}
            data['data'] = page_list.object_list
            data['Page_total'] = Paginator_list.count
            return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


    @csrf_exempt
    def post(self, request):
        try:
            data = {}
            type = request.data['type']
            if (type == 'change'):
                obj = Oil_Pipeline.objects.get(id=int(request.data['row']['id']))
                obj.pt1 = request.data['row']['pt1']
                obj.x1 = request.data['row']['x1']
                obj.y1 = request.data['row']['y1']
                obj.z1 = request.data['row']['z1']
                obj.tz1 = request.data['row']['tz1']
                obj.fsw1 = request.data['row']['fsw1']
                obj.jgcz1 = request.data['row']['jgcz1']
                obj.StartPointDepth = request.data['row']['StartPointDepth']
                obj.pt2 = request.data['row']['pt2']
                obj.x2 = request.data['row']['x2']
                obj.y2 = request.data['row']['y2']
                obj.z2 = request.data['row']['z2']
                obj.tz2 = request.data['row']['tz2']
                obj.fsw2 = request.data['row']['fsw2']
                obj.jgcz2 = request.data['row']['jgcz2']
                obj.EndPointDepth = request.data['row']['EndPointDepth']
                obj.BuriedWay = request.data['row']['BuriedWay']
                obj.Pipe = request.data['row']['Pipe']
                obj.Specification = request.data['row']['Specification']
                obj.StreetName = request.data['row']['StreetName']
                obj.ConstructionAge = request.data['row']['ConstructionAge']
                obj.OwnershipUnit = request.data['row']['OwnershipUnit']
                obj.BuriedDepth = request.data['row']['BuriedDepth']
                obj.LineCoding =  request.data['row']['LineCoding']
                obj.Modifiedate = request.data['row']['Modifiedate']
                obj.Remarks = request.data['row']['Remarks']
                obj.SHAPE_Leng = request.data['row']['SHAPE_Leng']
                obj.save()
                data = {}
                data['code'] = 200
                data['msg'] = '修改成功！'
                return JsonResponse(data)
            elif (type == 'delete'):
                data = {}
                obj = Oil_Pipeline.objects.get(id=int(request.data['value']))
                if (obj == None):
                    data['code'] = 210
                    data['msg'] = '没找到该台账信息！'
                    return JsonResponse(data)
                obj.delete()
                data['code'] = 200
                data['msg'] = '成功删除该记录！'

                obj = Oil_Pipeline.objects.all()
                objdata = Oil_PipelineSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(request.data['row_num']))
                page_list = Paginator_list.page(int(request.data['page']))
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)



#煤气管线
class Gaspipe_Api(APIView):
    def get(self, request):
        try:
            obj = GasPipeline.objects.all()
            objdata = GasPipelineSerializers(instance=obj, many=True)
            Paginator_list = Paginator(objdata.data, int(request.GET.get("row_num", "")))
            page_list = Paginator_list.page(int(request.GET.get("page", "")))
            data = {}
            data['data'] = page_list.object_list
            data['Page_total'] = Paginator_list.count
            return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


    @csrf_exempt
    def post(self, request):
        try:
            data = {}

            type = request.data['type']

            if(type == 'change'):
                obj = GasPipeline.objects.get(id=int(request.data['row']['id']))
                obj.other = request.data['row']['other']
                obj.pt1 = request.data['row']['pt1']
                obj.x1 = request.data['row']['x1']
                obj.y1 = request.data['row']['y1']
                obj.z1 = request.data['row']['z1']
                obj.tz1 = request.data['row']['tz1']
                obj.fsw1 = request.data['row']['fsw1']
                obj.jgcz1 = request.data['row']['jgcz1']
                obj.StartPointDepth = request.data['row']['StartPointDepth']
                obj.pt2 = request.data['row']['pt2']
                obj.x2 = request.data['row']['x2']
                obj.y2 = request.data['row']['y2']
                obj.z2 = request.data['row']['z2']
                obj.tz2 = request.data['row']['tz2']
                obj.fsw2 = request.data['row']['fsw2']
                obj.jgcz2 = request.data['row']['jgcz2']
                obj.EndPointDepth = request.data['row']['EndPointDepth']
                obj.BuriedWay = request.data['row']['BuriedWay']
                obj.Pipe = request.data['row']['Pipe']
                obj.Specification = request.data['row']['Specification']
                obj.StreetName = request.data['row']['StreetName']
                obj.ConstructionAge = request.data['row']['ConstructionAge']
                obj.OwnershipUnit = request.data['row']['OwnershipUnit']
                obj.BuriedDepth = request.data['row']['BuriedDepth']
                obj.LineCoding = request.data['row']['LineCoding']
                obj.Modifiedate = request.data['row']['Modifiedate']
                obj.Remarks = request.data['row']['Remarks']
                obj.SHAPE_Leng = request.data['row']['SHAPE_Leng']
                obj.save()
                data = {}
                data['code'] = 200
                data['msg'] = '修改成功！'
                return JsonResponse(data)
            elif(type == 'delete'):
                data = {}
                obj = GasPipeline.objects.get(id=int(request.data['value']))
                if(obj == None):
                    data['code'] = 210
                    data['msg'] = '没找到该台账信息！'
                    return JsonResponse(data)
                obj.delete()
                data['code'] = 200
                data['msg'] = '成功删除该记录！'

                obj = GasPipeline.objects.all()
                objdata = GasPipelineSerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(request.data['row_num']))
                page_list = Paginator_list.page(int(request.data['page']))
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
        except:
            data = {}
            data['code'] = 1000
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


# 污水管
class Sewage_Api(APIView):

    def get(self, request):
        obj = SewageLineLibrary.objects.all()
        objdata = SewageLineLibrarySerializers(instance=obj, many=True)
        Paginator_list = Paginator(objdata.data, int(request.GET.get("row_num", "")))
        page_list = Paginator_list.page(int(request.GET.get("page", "")))
        data = {}
        data['data'] = page_list.object_list
        data['Page_total'] = Paginator_list.count
        return JsonResponse(data)

    @csrf_exempt
    def post(self, request):
        try:
            data = {}
            type = request.data['type']
            if (type == 'change'):
                obj = SewageLineLibrary.objects.get(id=int(request.data['row']['id']))
                obj.pt1 = request.data['row']['pt1']
                obj.x1 = request.data['row']['x1']
                obj.y1 = request.data['row']['y1']
                obj.z1 = request.data['row']['z1']
                obj.tz1 = request.data['row']['tz1']
                obj.fsw1 = request.data['row']['fsw1']
                obj.jgcz1 = request.data['row']['jgcz1']
                obj.StartPointDepth = request.data['row']['StartPointDepth']
                obj.pt2 = request.data['row']['pt2']
                obj.x2 = request.data['row']['x2']
                obj.y2 = request.data['row']['y2']
                obj.z2 = request.data['row']['z2']
                obj.tz2 = request.data['row']['tz2']
                obj.fsw2 = request.data['row']['fsw2']
                obj.jgcz2 = request.data['row']['jgcz2']
                obj.EndPointDepth = request.data['row']['EndPointDepth']
                obj.BuriedWay = request.data['row']['BuriedWay']
                obj.Pipe = request.data['row']['Pipe']
                obj.Specification = request.data['row']['Specification']
                obj.StreetName = request.data['row']['StreetName']
                obj.ConstructionAge = request.data['row']['ConstructionAge']
                obj.OwnershipUnit = request.data['row']['OwnershipUnit']
                obj.BuriedDepth = request.data['row']['BuriedDepth']
                obj.LineCoding = request.data['row']['LineCoding']
                obj.Modifiedate = request.data['row']['Modifiedate']
                obj.Remarks = request.data['row']['Remarks']
                obj.SHAPE_Leng = request.data['row']['SHAPE_Leng']
                obj.save()
                data['code'] = 200
                data['msg'] = '修改成功！'
                return JsonResponse(data)
            elif (type == 'delete'):
                obj = SewageLineLibrary.objects.get(id=int(request.data['value']))
                if (obj == None):
                    data['code'] = 210
                    data['msg'] = '没找到该台账信息！'
                    return JsonResponse(data)
                obj.delete()
                data['code'] = 200
                data['msg'] = '成功删除该记录！'

                obj = SewageLineLibrary.objects.all()
                objdata = SewageLineLibrarySerializers(instance=obj, many=True)
                Paginator_list = Paginator(objdata.data, int(request.data['row_num']))
                page_list = Paginator_list.page(int(request.data['page']))
                data['data'] = page_list.object_list
                data['Page_total'] = Paginator_list.count
                return JsonResponse(data)
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


class EditRole_Api(APIView):
    def get(self, request):
        try:
            obj = Constructionledger.objects.all()
            data = ConstructionledgerSerializers(instance=obj, many=True)
            return JsonResponse({'data': data.data})
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


    @csrf_exempt
    def post(self, request):
        try:
            pass
        except:
            data = {}
            data['code'] = 200
            data['msg'] = '服务器繁忙请稍后再试！'
            return JsonResponse(data)


# 上传文件
class Upfile_api(APIView):
    def get(self, request):
        try:
            ModelName = request.GET.get('ModelName', '')
            if ModelName == '':
                return JsonResponse({'data': "模型名为空！"})

            return JsonResponse({'code': 0, 'data': list(DescriptionFile.objects.filter(ModelName=ModelName).values())})
        except:
            return JsonResponse({'data': "服务器内部错误！"})


    @csrf_exempt
    def post(self, request):
        try:
            ModelName = request.GET.get('ModelName', '')
            DescriptionFile.objects.create(
                ModelName=ModelName,
                file_name=request.data["file"].name,
                dockerFile=request.data["file"]
            )

            return JsonResponse({'data': "hello"})

        except:
            return JsonResponse({'data': "error"})


# 下载文件
class Download_Api(APIView):
    def get(self, request):
        return JsonResponse({'code': 210, 'msg': '服务器内部错误！'})


    @csrf_exempt
    def post(self, request):
        try:
            id = int(request.data['id'])
            flie_obj = DescriptionFile.objects.get(id=id)
            download_path = BASE_DIR + '/' + flie_obj.dockerFile.name
            print(download_path)
            file = open(download_path, 'rb')
            response = FileResponse(file)
            response['Content-Type'] = 'application/octet-stream'
            response['Content-Disposition'] = "attachment; filename= {}".format(flie_obj.file_name)
            return response
        except:
            return JsonResponse({'code': 210, 'msg' : '服务器内部错误！'})



 # 对接后端的 ERP   设备
class ERP_Device_Api(APIView):
    def get(self, request):
        data = {}
        try:
            http = urllib3.PoolManager()
            r = http.request('GET', 'http://127.0.0.1:8000/ERP_Houses_Api')
            print(r._body)
            test_data = r.data.decode("utf-8").strip('"')
            test_data = json.loads(test_data)
            test_data.replace('\\"', '"')
            return JsonResponse({'data': test_data})
        except:
            pass


    @csrf_exempt
    def post(self, request):
        return JsonResponse({'data': "ERP_Api ok"})





 # 对接后端的 ERP 系统   建筑
class ERP_Houses_Api(APIView):
    def get(self, request):
        data = {}
        data['hello'] = "test"
        return HttpResponse("\"{\"test\":\"test\"}\"")


    @csrf_exempt
    def post(self, request):
        return JsonResponse({'data': "ERP_Api ok"})


    