# Generated by Django 2.0 on 2019-10-30 09:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Other', '0003_auto_20191030_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='devicemanagement',
            name='TimeOfUse',
            field=models.CharField(default='', max_length=30, verbose_name='使用时间'),
        ),
        migrations.AlterField(
            model_name='devicemanagement',
            name='UniformNumbers',
            field=models.CharField(default='', max_length=20, verbose_name='模型名-唯一'),
        ),
        migrations.AlterField(
            model_name='devicemanagement',
            name='UseUnit',
            field=models.CharField(default='', max_length=30, verbose_name='使用单位'),
        ),
        migrations.AlterField(
            model_name='devicemanagement',
            name='specification',
            field=models.CharField(default='', max_length=30, verbose_name='规格型号'),
        ),
    ]
