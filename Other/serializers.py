# _*_encoding:utf-8 _*_

from rest_framework import serializers
from Other.models import Constructionledger, DeviceManagement, Oil_Pipeline ,GasPipeline, SewageLineLibrary


class ConstructionledgerSerializers(serializers.ModelSerializer):
    class Meta:
        model = Constructionledger
        fields = "__all__"


class DeviceManagementSerializers(serializers.ModelSerializer):
    class Meta:
        model = DeviceManagement
        fields = "__all__"


class Oil_PipelineSerializers(serializers.ModelSerializer):
    class Meta:
        model = Oil_Pipeline
        fields = "__all__"


class GasPipelineSerializers(serializers.ModelSerializer):
    class Meta:
        model = GasPipeline
        fields = "__all__"


class SewageLineLibrarySerializers(serializers.ModelSerializer):
    class Meta:
        model = SewageLineLibrary
        fields = "__all__"

